const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const baseDir = process.cwd();
const nodeModulesPath = path.resolve(baseDir, 'node_modules');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');

module.exports = {
  entry: path.resolve(__dirname, "./src/index.js"),
  output: {
    path: path.join(__dirname, "public"),
    filename: "index.js"
  },
  devServer: {
    contentBase: './public/',
    port: 8080,
    historyApiFallback: true,
    disableHostCheck: true
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: ["babel-loader"],
      },
      {
        test: /\.pug$/,
        use: ['html-loader?attrs=false', 'pug-html-loader']
      },
      {
        test: /\.(sa|sc|c)ss$/,
        use: [
         MiniCssExtractPlugin.loader,
          {
            loader: 'css-loader',
            options: {
              url: false,
              // modules: true,
              // sourceMap: true,
              // root: '/vendors/',
              // localIdentName: '[local]',
            },
          },
          'postcss-loader',
          'sass-loader',
        ]
      },
      {
        test: /\.(eot|svg|ttf|woff|woff2)$/,
        loader: 'file-loader',
        options: {
          name: '[name].[ext]',
          outputPath: 'assets'
        }
      },
      {
        test: /\.(png|jpg|gif)$/,
        loader: 'file-loader',
        options: {
          name: '[name].[ext]',
          outputPath: 'assets',
          publicPath: path.resolve(baseDir, `${baseDir}/src/assets`),
        },
      },
    ]
  },
  resolve: {
    extensions: ['.js', '.jsx'],
    modules: [
      path.resolve(baseDir, `${baseDir}`),
      path.resolve(baseDir, `${baseDir}/src`),
      path.resolve(baseDir, `${baseDir}/src/assets`),
      nodeModulesPath,
    ],
  },
  optimization: {
    minimizer: [new TerserPlugin({}), new OptimizeCSSAssetsPlugin({})],
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: "./src/index.pug",
      filename: "index.html",
      favicon: "./src/simple-logo.png"
    }),
    new HtmlWebpackPlugin({
      inject: false,
      template: "./src/assets/include/card.html",
      filename: "./assets/include/card.html",
    }),
    new MiniCssExtractPlugin({
      filename: 'index.css'
    }),
    new CopyPlugin([
      {
        from: 'src/assets/images',
        to: 'assets/images/'
      },
    ]),
    new CopyPlugin([
      {
        from: 'src/assets/fonts',
        to: 'assets/fonts/'
      },
    ]),
  ]
};