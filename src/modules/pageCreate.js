import getJokes from "./getJokes";

const pageCreate = (dataValue) => {
    const countElement = document.querySelector('.searchResults__count');
    const totalElement = document.querySelector('.searchResults__total');
    const requestElement = document.querySelector('.searchResults__request');
    const inputSearch = document.querySelector('input[name=jokeSearch]');
    const totalItem = dataValue.result.length;

    let firstItem;
    let lastItem;
    let remainderItems = totalItem%10;
    let remainderSlide = ((totalItem - (Number(localStorage.getItem('lastCard'))+1)));

    if(localStorage.getItem('firstCard')) {
        firstItem = Number(localStorage.getItem('firstCard'));
    } else {
        firstItem = 0;
    }
    if(localStorage.getItem('lastCard')) {
        lastItem = Number(localStorage.getItem('lastCard'));
    } else {
        lastItem = 9;
    }

    let results = [];

    for(let firstItem = 0; firstItem <= lastItem; firstItem++) {
        results.push(dataValue.result[firstItem]);
        getJokes(dataValue.result[firstItem]);
    }

    countElement.innerHTML = `1-${lastItem+1}`
    totalElement.innerHTML = totalItem;
    requestElement.innerHTML = `\'${inputSearch.value}\'`

    if (remainderSlide >= 10) {
        localStorage.setItem('firstCard', `${firstItem+10}`);
        localStorage.setItem('lastCard', `${lastItem+10}`);
    } else if (remainderSlide < 10 && remainderSlide !== 0) {
        localStorage.setItem('firstCard', `${firstItem+10}`);
        localStorage.setItem('lastCard', `${lastItem+remainderSlide}`);
    } else {
        document.querySelector('.searchResults__click').classList.add('searchResults__click--hidden');
    }
    document.querySelector('.searchResults').classList.add('searchResults--show');
    return results;
}

export default pageCreate;