const favouriteShow = (event) => {
    const target = event.target;
    event.stopPropagation();
    const showElement = document.querySelector('.favourite');
    //showElement.classList.toggle('favourite--show');
    if(showElement.classList.contains('favourite--show')) {
        showElement.classList.add('favourite--hidden');
        setTimeout(() => {
            showElement.classList.remove('favourite--show');
            showElement.classList.remove('favourite--hidden');
        }, 300)
    //
    } else {
        showElement.classList.add('favourite--show');
    }
    document.getElementsByTagName('body')[0].classList.toggle('bg');
};

export default favouriteShow;