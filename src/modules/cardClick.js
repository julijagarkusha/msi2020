import {jokesArray} from './getJokes';
import cardCreate from "./cardCreate";

let favouriteJokes = [];
let likedJoke;
let replayJoke;
const cardLiked = document.querySelector('.card--liked');

const cardClick = (event) => {
    const target = event.target;
    let saveJokes = JSON.parse(localStorage.getItem('favouriteJokes'));
    if(target.classList.contains('card--select') && !target.classList.contains('card--selectBg')) {
        target.classList.add('card--selectBg');
        const cardId = target.parentNode.getAttribute('id');
        likedJoke = jokesArray.find((item)=> {
            return item.id === cardId;
        })

        replayJoke = favouriteJokes.find(favouriteJoke => {
            return (favouriteJoke.id === likedJoke.id);
        });
        if(favouriteJokes.length !== 0 && !replayJoke) {
            favouriteJokes.push(likedJoke);
        } else if (favouriteJokes.length === 0 && !replayJoke) {
            favouriteJokes.push(likedJoke);
        } else {
            return false;
        }

        if(saveJokes && saveJokes[0] !== null) {
            cardCreate(favouriteJokes[favouriteJokes.length-1], cardLiked);

            let allJokes = saveJokes.concat(favouriteJokes[favouriteJokes.length - 1]);
            localStorage.setItem('favouriteJokes', JSON.stringify(allJokes));
        } else {
            localStorage.setItem('favouriteJokes', JSON.stringify(favouriteJokes));
            cardCreate(favouriteJokes[favouriteJokes.length-1], cardLiked);
        }

    } else if (target.classList.contains('card--select') && target.classList.contains('card--selectBg')) {
        let deleteCardId = String(target.parentNode.id);
        let deleteCard = document.querySelectorAll('.card--liked .card');

        target.classList.remove('card--selectBg');
        deleteCard.forEach(card => {
            if(card.id === deleteCardId) {
                card.remove();
            }
        })

        saveJokes = saveJokes.filter(item => item.id !== deleteCardId);
        localStorage.setItem('favouriteJokes', JSON.stringify(saveJokes));

        likedJoke = {};
        replayJoke = false;

        favouriteJokes = favouriteJokes.filter(item => item.id !== deleteCardId);

        return false;
    } else {
        return false;
    }
}

export default cardClick;