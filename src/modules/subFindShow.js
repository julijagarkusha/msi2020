import errorTextHidden from "./errorTextHidden";

const subFindElements = document.querySelectorAll('.findSub');
const subFindHidden = () => {
    subFindElements.forEach(subFindElement => {
        subFindElement.classList.remove('findSub--show')
    })
    errorTextHidden();
}

const subFindShow = (event) => {
    const target = event.target;
    subFindHidden();
    document.querySelector('.searchResults').classList.remove('searchResults--show');
    document.querySelector('.find__joke .button--submit').removeAttribute('disabled');
    document.querySelector('.searchResults__notFound').classList.remove('searchResults__notFound--show');
    document.querySelector('.searchResults__click').classList.remove('searchResults__click--hidden');
  //  document.querySelector('input[name=jokeSearch]').value = "";
  //  document.querySelector('.find__joke .button--submit').setAttribute('disabled', 'false');
    const subFindTarget = target.parentNode.parentNode.querySelector('.findSub');
    if(subFindTarget) {
        subFindTarget.classList.add('findSub--show');
    }
}

export default subFindShow;