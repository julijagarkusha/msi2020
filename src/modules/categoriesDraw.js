const categoryList = document.querySelector('.category__list');

const createCategory = (categoryValue) => {
    let newCategory = document.createElement('button');
    newCategory.innerHTML = categoryValue;
    newCategory.classList.add('category__item');
    newCategory.classList.add('button');
    newCategory.setAttribute('data-category', categoryValue);
    categoryList.appendChild(newCategory);
}

const categoriesDraw = () => {
    fetch('https://api.chucknorris.io/jokes/categories')
        .then((response) => {
            return response.json();
        })
        .then((data) => {
            categoryList.innerHTML = "";
            data.forEach(category => {
                createCategory(category);
            })
        });
};

export default categoriesDraw;