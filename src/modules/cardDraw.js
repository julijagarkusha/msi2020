import cardCreate from "./cardCreate";
import getJokes from './getJokes';
import pageCreate from './pageCreate';

const errorHandler = (errorValue, errorMessage) => {
    if (errorValue === 404 && errorMessage === 'Not Found') {
        document.querySelector('.category__error').classList.add('error__message--show');
    } else if (errorValue === 400 && errorMessage === 'Bad Request') {
        document.querySelector('.search__error').classList.add('error__message--show');
    }
}
const cardsGet = document.querySelector('.card--get');

const cardDraw = (url) => {
    fetch(url)
        .then(response => {
            return response.json();
        })
        .then(data => {
            if(data.result && data.result.length > 10) {
               let dataItems = pageCreate(data);
               dataItems.forEach(dataItem => {
                   cardCreate(dataItem, cardsGet);
               });
            } else if (data.result && data.result.length < 10 && data.result.length !== 0) {
                data.result.forEach(dataItem => {
                    cardCreate(dataItem, cardsGet);
                    getJokes(dataItem);
                });
            } else if (data.result && data.result.length === 0) {
                document.querySelector('.searchResults__notFound').classList.add('searchResults__notFound--show');
            } else {
                data.status ? errorHandler(data.status, data.error) : cardCreate(data, cardsGet);
                getJokes(data);
            }
        })
}

export default cardDraw;