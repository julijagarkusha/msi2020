import moment from "moment";

const cardCreate = (response, container) => {
    if(!localStorage.getItem('cardHTML')) {
        fetch('./assets/include/card.html')
            .then(response => {
                return response.text();
            })
            .then(data => {
                localStorage.setItem('cardHTML', data);
            })
    }

    let cardHTML = localStorage.getItem('cardHTML');
    let newCard = document.createElement('article');

    newCard.classList.add('card');
    container.classList.contains('card--get') ? newCard.classList.add('card--big') : '';
    newCard.innerHTML = cardHTML;
    newCard.setAttribute('id', response.id);
    newCard.querySelector('.card__joke').innerHTML = response.value;
    if(response.categories !== undefined && response.categories.length) {
        newCard.querySelector('.card__category').innerHTML = response.categories
    } else {
        newCard.querySelector('.card__category').innerHTML = 'without category'
    }
    newCard.querySelector('.card__id > a').setAttribute('href', response.icon_url);
    newCard.querySelector('.card__id > a').setAttribute('target', '_blank');
    newCard.querySelector('.card__id > a').innerHTML = response.id;
    let hoursAgo = moment().diff(moment(response.updated_at), 'hours');
    newCard.querySelector('.card__time').innerHTML = `${hoursAgo} hours ago`;
    container.appendChild(newCard);
}

export default cardCreate;