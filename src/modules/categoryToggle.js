import errorTextHidden from "./errorTextHidden";

const categoryToggle = (event) => {
    event.preventDefault();
    const target = event.target;
    errorTextHidden();
    const allCategories = target.parentNode.childNodes;
    const formElement = document.querySelector('.find__joke');
    const category = target.getAttribute('data-category');
    allCategories.forEach(categoryItem => {
        categoryItem.classList.remove('button--active');
    });
    target.classList.add('button--active');
    formElement.setAttribute('data-category', category);
}

export default categoryToggle;