import "./styles/index.scss";
import categoriesDraw from "./modules/categoriesDraw";
import subFindShow from "./modules/subFindShow";
import categoryToggle from "./modules/categoryToggle";
import cardDraw from "./modules/cardDraw";
import errorTextHidden from "./modules/errorTextHidden";
import favouriteShow from "./modules/favouriteShow";
import cardClick from "./modules/cardClick";
import cardCreate from "./modules/cardCreate";

const randomFind = document.getElementById('jokeRandom');
const categoryFind = document.getElementById('jokeFromCategory');
const searchFind = document.getElementById('jokeSearch');
const radioButtons = document.querySelectorAll('.radioButton input');
const categoryElements = document.querySelectorAll('.category__list');
const jokeGetMethods = document.querySelectorAll('.radioButton > input');
const formElement = document.querySelector('.find__joke');
const inputSearch = document.querySelector('input[name=jokeSearch]');
const showMoreSearch = document.querySelector('.searchResults__click');
const jokeGetButton = document.querySelector('.find__joke .button--submit');
const favouriteToggle = document.querySelector('.favourite__toggle');
const showElement = document.querySelector('.favourite');
const bodyElement = document.getElementsByTagName('body')[0];
const likedIcons = document.querySelectorAll('.card--get');
const cardLiked = document.querySelector('.card--liked');

sessionStorage.setItem('currentValue', 'value');

let saveJokes = JSON.parse(localStorage.getItem('favouriteJokes'));

document.addEventListener('DOMContentLoaded', ()=> {
    if(saveJokes && saveJokes[0] !== null) {
        saveJokes.forEach(saveJoke => {
            cardCreate(saveJoke, cardLiked);
        })
    }
})

categoriesDraw();

radioButtons.forEach(radioButton => {
    radioButton.addEventListener('change', subFindShow)
})

categoryElements.forEach(categoryElement => {
    categoryElement.addEventListener('click', categoryToggle)
})

const jokeGet = (event) => {
    event.preventDefault();
    const jokeRandom = 'https://api.chucknorris.io/jokes/random';
    const jokeCategory = 'https://api.chucknorris.io/jokes/random?category=';
    const jokeSearch = 'https://api.chucknorris.io/jokes/search?query=';
    const jokeGetElement = [...jokeGetMethods].find((jokeGetMethod)=> {
        return jokeGetMethod.checked;
    });
    const jokeGetMethod = jokeGetElement.getAttribute('data-name');
    const categoryName = formElement.getAttribute('data-category');
    let searchValue = document.querySelector('input[name=\"jokeSearch\"]').value;

    switch (jokeGetMethod) {
        case 'random' : cardDraw(jokeRandom); break;
        case 'category' : cardDraw(`${jokeCategory}${categoryName}`); break;
        case 'search' :
            cardDraw(`${jokeSearch}${searchValue}`);
            sessionStorage.setItem('searchValue', searchValue);
            if(sessionStorage.getItem('searchValue') !== sessionStorage.getItem('currentValue')) {
            } else {
                jokeGetButton.setAttribute('disabled', 'disabled');
            }
            break;
    }
}

jokeGetButton.addEventListener('click', jokeGet);

showMoreSearch.addEventListener('click', jokeGet);

inputSearch.addEventListener('input', errorTextHidden);
inputSearch.addEventListener('input', ()=> {
    localStorage.removeItem('firstCard');
    localStorage.removeItem('lastCard');
    sessionStorage.setItem('currentValue', inputSearch.value);
    document.querySelector('.searchResults').classList.remove('searchResults--show');
    jokeGetButton.removeAttribute('disabled');
    document.querySelector('.searchResults__notFound').classList.remove('searchResults__notFound--show');
    document.querySelector('.searchResults__click').classList.remove('searchResults__click--hidden');
});

favouriteToggle.addEventListener('click', favouriteShow);

likedIcons.forEach(likedIcon => {
    likedIcon.addEventListener('click', cardClick);
})

document.addEventListener('click', (event) => {
    if(showElement.classList.contains('favourite--show')) {
        showElement.classList.add('favourite--hidden');
        setTimeout(() => {
            showElement.classList.remove('favourite--show');
            showElement.classList.remove('favourite--hidden');
        }, 300)
    }
    document.getElementsByTagName('body')[0].classList.remove('bg');
})
